use rand::prelude::*;

struct WordGenerator {
    ngram: Ngram,
    tail: String,
}

impl WordGenerator {
    fn generate(&mut self, size: u64) -> String {
        if self.tail.len() < 2 {
            self.tail.push_str(&self.ngram.gen_char("").to_string());
            self.tail
                .push_str(&self.ngram.gen_char(&self.tail).to_string());
        }
        let mut generated = self.tail.clone();
        for _ in 0..size {
            let next_char = self.ngram.gen_char(&self.tail);
            generated.push(next_char);
            self.tail = self.tail[1..].to_string();
            self.tail.push(next_char);
        }
        generated
    }
}

#[derive(Debug)]
struct Ngram {
    hits: i64,
    level: u8,
    following: std::collections::HashMap<u8, Ngram>,
}

impl Ngram {
    fn hit(&mut self, something: &str) {
        self.hits += 1;
        if self.level > 0 {
            self.following
                .entry(something.bytes().nth(0).unwrap())
                .or_insert(Ngram {
                    hits: 0,
                    level: self.level - 1,
                    following: std::collections::HashMap::new(),
                })
                .hit(&something[1..])
        }
    }

    fn gen_char(&self, something: &str) -> char {
        if something.len() > 0 {
            let get_ngram = self.following.get(&something.bytes().nth(0).unwrap());
            match get_ngram {
                Some(next_ngram) => return next_ngram.gen_char(&something[1..]),
                None => (),
            }
        }
        let mut rng = rand::thread_rng();
        let mut rand_val = (self.hits as f64 * rng.gen::<f64>()) as i64;
        for (character, ngram) in self.following.iter() {
            rand_val -= ngram.hits;
            if rand_val < 0 {
                return *character as char;
            }
        }
        ' '
    }
}

fn main() {
    let my_str = "It wasn't always so clear, but the Rust programming language is fundamentally about empowerment: no matter what kind of code you are writing now, Rust empowers you to reach farther, to program with confidence in a wider variety of domains than you did before. Take, for example, \"systems-level\" work that deals with low-level details of memory management, data representation, and concurrency. Traditionally, this realm of programming is seen as arcane, accessible only to a select few who have devoted the necessary years learning to avoid its infamous pitfalls. And even those who practice it do so with caution, lest their code be open to exploits, crashes, or corruption. Rust breaks down these barriers by eliminating the old pitfalls and providing a friendly, polished set of tools to help you along the way. Programmers who need to \"dip down\" into lower-level control can do so with Rust, without taking on the customary risk of crashes or security holes, and without having to learn the fine points of a fickle toolchain. Better yet, the language is designed to guide you naturally towards reliable code that is efficient in terms of speed and memory usage. Programmers who are already working with low-level code can use Rust to raise their ambitions. For example, introducing parallelism in Rust is a relatively low-risk operation: the compiler will catch the classical mistakes for you. And you can tackle more aggressive optimizations in your code with the confidence that you won't accidentally introduce crashes or vulnerabilities. But Rust isn't limited to low-level systems programming. It's expressive and ergonomic enough to make CLI apps, web servers, and many other kinds of code quite pleasant to write - you'll find simple examples of both later in the book. Working with Rust allows you to build skills that transfer from one domain to another; you can learn Rust by writing a web app, then apply those same skills to target your Raspberry Pi. This book fully embraces the potential of Rust to empower its users. It's a friendly and approachable text intended to help you level up not just your knowledge of Rust, but also your reach and confidence as a programmer in general. So dive in, get ready to learn-and welcome to the Rust community!";
    let as_vec = my_str.as_bytes();
    let mut strawberry = Ngram {
        hits: 0,
        level: 3,
        following: std::collections::HashMap::new(),
    };
    for trigram in as_vec.windows(3) {
        let as_str_again = std::str::from_utf8(trigram).unwrap();
        strawberry.hit(as_str_again);
    }
    let mut wg = WordGenerator {
        ngram: strawberry,
        tail: "".to_string(),
    };
    println!("{}", wg.generate(80))
}
